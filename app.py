#import numpy as np
from flask import Flask, request, jsonify, render_template, url_for
#import pickle


app = Flask(__name__)
#model = pickle.load(open('randomForestRegressor.pkl','rb'))


@app.route('/')
def home():
	#return 'Hello World'
	return render_template('home.html')
	#return render_template('index.html')

@app.route('/predict',methods = ['POST'])
def predict():
	prediction = 10 
	#output = round(prediction[0], 2)
	return render_template('home.html', prediction_text="AQI for Jaipur {}".format(prediction))

if __name__ == '__main__':
	app.run(port = 8000, debug = False)
